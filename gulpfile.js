const gulp = require('gulp');

const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const concatCss = require('gulp-concat-css');
const minify = require('gulp-minify-css');


gulp.task('sass', function () {
    return gulp.src('sources/scss/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(concatCss('sources/css/style.css'))
        .pipe(minify({
            keepBreaks: true
        }))
        .pipe(gulp.dest('.'));
});


// gulp.task('default', function(){
//     gulp.watch(['sources/scss/**/*.scss'], ['sass']);
//
// })
gulp.task('default', function () {
    gulp.watch('sources/scss/**/*.scss', gulp.series('sass'));
});