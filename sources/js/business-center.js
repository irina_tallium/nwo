$(document).ready(function () {

    $('.slider-business-center').slick({
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000
    })

    $('.slider-popup').slick({})


    $("#playButton").click(function(){
        let video = $("video").get(0);
        $(".content-video").hide();
        video.play();
    });

    $("video").click(function(){
        $(".content-video").show();
        $(this).get(0).pause();
    })

})


function mapBusinessCenter() {

    var element = document.getElementById("map_business_center");
    var options = {
        center: {lat: 47.5110774, lng: 19.0694233},
        zoom: 12
    }

    var myMap = new google.maps.Map(element, options);

    var markers = [
        {
            coord: {lat: 47.4895215, lng: 19.0675473},
            image: "img/workspace-solution-item/pin-default.svg",
            info: "<img src=\"img/workspace-solution-item/image-2.png\" style=\"width: 100px; height: auto\"/><p>B52</p>"
        }
    ];

    function addMarkerBusinessCenter(properties) {
        var marker = new google.maps.Marker({
            position: properties.coord,
            map: myMap,
            icon: properties.image,
            animation: google.maps.Animation.DROP
        });
        if (properties.image) {
            marker.setIcon(properties.image);
        }

        if (properties.info) {
            var infoWindow = new google.maps.InfoWindow({
                content: properties.info
            });
            marker.addListener("click", function () {
                marker.setIcon('img/workspace-solution-item/pin-active.svg');
                infoWindow.open(myMap, marker);
            })
        }
    }


    for (var i = 0; i < markers.length; i++) {
        addMarkerBusinessCenter(markers[i]);
    }

}

mapBusinessCenter();
