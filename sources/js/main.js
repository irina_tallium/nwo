$(document).ready(function () {

    $('.corporate-slider').slick({
        dots: true
    });

    $('.slider-popup').slick({
        dots: true
    });


    $('.gallery-cities').slick({
        dots: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1015,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('select').niceSelect();

});

function initMap() {
    var element = document.getElementById("map");
    var options = {
        center: {lat: 47.5110774, lng: 19.0694233},
        zoom: 12
    }

    var myMap = new google.maps.Map(element, options);

    var markers = [
        {
            coord: {lat: 47.5110774, lng: 19.0694233},
            image: "img/workspace-solution-item/pin-default.svg",
            info: "<img src=\"img/workspace-solution-item/image-1.png\" style=\"width: 100px; height: auto\"/><p>A100 Business center</p>"
        },
        {
            coord: {lat: 47.4981287, lng: 19.0527851},
            image: "img/workspace-solution-item/pin-default.svg",
            info: "<img src=\"img/workspace-solution-item/image.png\" style=\"width: 100px; height: auto\"/><p>Anker</p>"
        },
        {
            coord: {lat: 47.4895215, lng: 19.0675473},
            image: "img/workspace-solution-item/pin-default.svg",
            info: "<img src=\"img/workspace-solution-item/image-2.png\" style=\"width: 100px; height: auto\"/><p>B52</p>"
        },
        {
            coord: {lat: 47.504828, lng: 19.0524583},
            image: "img/workspace-solution-item/pin-default.svg",
            info: "<img src=\"img/workspace-solution-item/image-3.png\" style=\"width: 100px; height: auto\"/><p>BJ48</p>"
        },
        {
            coord: {lat: 47.529613, lng: 19.0369215},
            image: "img/workspace-solution-item/pin-default.svg",
            info: "<img src=\"img/workspace-solution-item/image-2.png\" style=\"width: 100px; height: auto\"/><p>BSQ</p>"
        },
        {
            coord: {lat: 47.4558086, lng: 18.9353495},
            image: "img/workspace-solution-item/pin-default.svg",
            info: "<img src=\"img/workspace-solution-item/image-3.png\" style=\"width: 100px; height: auto\"/><p>Budaörs</p>"
        }
    ];

    function addMarker(properties) {
        var marker = new google.maps.Marker({
            position: properties.coord,
            map: myMap,
            icon: properties.image,
            animation: google.maps.Animation.DROP
        });
        if (properties.image) {
            marker.setIcon(properties.image);
        }

        if (properties.info) {
            var infoWindow = new google.maps.InfoWindow({
                content: properties.info
            });
            marker.addListener("click", function () {
                marker.setIcon('img/workspace-solution-item/pin-active.svg');
                infoWindow.open(myMap, marker);
            })
        }
    }


    for (var i = 0; i < markers.length; i++) {
        addMarker(markers[i]);
    }

}

initMap();

